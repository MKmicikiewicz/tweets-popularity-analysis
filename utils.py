import datetime
import pandas as pd
import os
import tweepy
from config import get_config
import emoji
import regex
import json


def get_time_boundaries(n_days):
    end_time = datetime.datetime.utcnow() - datetime.timedelta(days=1) # newest tweets
    start_time = end_time - datetime.timedelta(days=n_days) # oldest tweets
    return end_time, start_time


def time_export(dct):
    dct['time_of_day'] = list[str]()
    dct['day_of_week'] = list[str]()
    weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    for t in dct['time']:
        if t.hour < 7:
            dct['time_of_day'].append('Night')
        elif 7 <= t.hour < 12:
            dct['time_of_day'].append('Morning')
        elif 12 <= t.hour < 19:
            dct['time_of_day'].append('Afternoon')
        else:
            dct['time_of_day'].append('Evening')
        dct['day_of_week'].append(weekdays[datetime.date.weekday(t)])


def save_to_csv(file_name, df):
    input_path = os.path.join(os.getcwd(), 'data')
    assert file_name.endswith('.csv'), 'add extension to the file name'
    assert os.path.relpath(input_path, os.getcwd()) == 'data', "Invoke this function from main dir"
    out_path = os.path.join('data', file_name)
    df.to_csv(out_path, mode='a', header=not os.path.exists(out_path), index=False)


def get_semantic_target(dct, query):
    n = len(dct['time'])
    dct['query'] = [[]]*n
    dct['query'][0].append(list(query.split()[:query.split().index('lang:en')]))


def prepare_holder():
    tweet_dct = dict()
    user_dct = dict()
    tweet_dct['Tweet_ID'] = list[int]()
    tweet_dct['Author_ID'] = list[int]()
    tweet_dct['time'] = list[datetime.datetime]()
    tweet_dct['text'] = list[str]()
    tweet_dct['n_retweets'] = list[int]()
    tweet_dct['n_replies'] = list[int]()
    tweet_dct['n_likes'] = list[int]()
    tweet_dct['n_quotes'] = list[int]()
    tweet_dct['n_media'] = list[int]()
    tweet_dct['n_photo'] = list[int]()
    tweet_dct['n_video'] = list[int]()
    tweet_dct['n_gif'] = list[int]()
    tweet_dct['is_retweet'] = list[bool]()
    tweet_dct['is_quote'] = list[bool]()
    tweet_dct['is_replying'] = list[bool]()
    tweet_dct['is_poll'] = list[bool]()
    user_dct['Author_ID'] = list[int]()
    user_dct['is_verified'] = list[bool]()
    user_dct['n_followers'] = list[int]()
    return tweet_dct, user_dct


def features_to_csv(filename, query, n_tweets, n_days):
    tweet_dct, user_dct = prepare_holder()
    paginator = get_recent_tweets(query, n_tweets, n_days)
    tweet_dct, user_dct = extract_features(paginator, tweet_dct, user_dct)
    time_export(tweet_dct)
    get_semantic_target(tweet_dct, query)
    tweet_df = pd.DataFrame(data=tweet_dct)
    user_df = pd.DataFrame(data=user_dct)
    final_df = pd.merge(tweet_df, user_df, on='Author_ID')
    final_df.drop_duplicates(subset=['Tweet_ID'], inplace=True)
    final_df['target'], final_df['semantic'] = zip(*final_df['query'].apply(func=query_extract))
    final_df['n_emojis'], final_df['n_slang'], final_df['n_punct'], final_df['n_tags'], final_df['n_annot'], final_df['n_links'], final_df['n_words'], final_df['text_length'], final_df['text'] = zip(*final_df['text'].apply(text_extract))
    final_df['score'] = final_df[['n_likes', 'n_retweets', 'n_quotes', 'n_replies', 'n_followers']].apply(lambda x: make_score(*x), axis=1)
    save_to_csv(filename, final_df)


def get_recent_tweets(query, n_tweets, n_days):
    client = tweepy.Client(get_config()[-1])
    end_time, start_time = get_time_boundaries(n_days)
    limit = n_tweets//100
    paginator_iterator = tweepy.Paginator(client.search_recent_tweets,
                                     limit=limit,
                                     query=query,
                                     max_results=100,
                                     end_time=end_time,
                                     start_time=start_time,
                                     tweet_fields=['created_at',
                                                   'lang',
                                                   'public_metrics'],
                                     user_fields=['public_metrics',
                                                  'verified'],
                                     expansions=['author_id',
                                                 'attachments.media_keys',
                                                 'attachments.poll_ids',
                                                 'referenced_tweets.id'])
    return paginator_iterator


def extract_features(paginator, tweet_dct, user_dct):
    for response in paginator:
        if response.data is not None:
            for data in response.data:
                tweet_dct['Tweet_ID'].append(data.id)
                tweet_dct['Author_ID'].append(data.author_id)
                tweet_dct['time'].append(data.created_at)
                tweet_dct['text'].append(data.text)
                tweet_dct['n_retweets'].append(data.public_metrics['retweet_count'])
                tweet_dct['n_replies'].append(data.public_metrics['reply_count'])
                tweet_dct['n_likes'].append(data.public_metrics['like_count'])
                tweet_dct['n_quotes'].append(data.public_metrics['quote_count'])
                photo_count = 0
                video_count = 0
                gif_count = 0
                if data.attachments:
                    tweet_dct['is_poll'].append(True if 'poll_ids' in data.attachments.keys() else False)
                    if 'media_keys' in data.attachments.keys():
                        tweet_dct['n_media'].append(len(data.attachments['media_keys']))
                        for key in data.attachments['media_keys']:
                            for media in response.includes['media']:
                                if key == media.media_key:
                                    if media.type == 'video':
                                        video_count += 1
                                    elif media.type == 'photo':
                                        photo_count += 1
                                    elif 'gif' in media.type:
                                        gif_count += 1
                                    else:
                                        print(media.type)
                    else:
                        tweet_dct['n_media'].append(0)
                else:
                    tweet_dct['is_poll'].append(False)
                    tweet_dct['n_media'].append(0)
                tweet_dct['n_photo'].append(photo_count)
                tweet_dct['n_video'].append(video_count)
                tweet_dct['n_gif'].append(gif_count)
                quote = False
                retweet = False
                reply = False
                if data.referenced_tweets:
                    for ref in data.referenced_tweets:
                        if ref.type == 'quoted':
                            quote = True
                        elif ref.type == 'retweeted':
                            retweet = True
                        elif ref.type == 'replied_to':
                            reply = True
                tweet_dct['is_replying'].append(reply)
                tweet_dct['is_retweet'].append(retweet)
                tweet_dct['is_quote'].append(quote)
        else:
            raise ValueError('response data is empty - investigate')
        if response.includes['users'] is not None:
            for user in response.includes['users']:
                user_dct['Author_ID'].append(user.id)
                user_dct['is_verified'].append(user.verified)
                user_dct['n_followers'].append(user.public_metrics['followers_count'])
        else:
            raise ValueError('users data is empty - investigate')
    return tweet_dct, user_dct


def text_extract(text):
    lst = list[str]()
    punctuation =  ['.', ',', ';', '!', '?', '(', '"', '-', ':']
    with open(r'data/slang_list.json', 'r') as f:
        dct = json.load(f)
    slang_words = list(dct.keys())
    emoji_counter = 0
    link_counter = 0
    annot_counter = 0
    tags_counter = 0
    punctuation_counter = 0
    slang_counter = 0
    data = regex.findall(r'\X', text)
    for d in data:
        if emoji.is_emoji(d):
            emoji_counter += 1

    for word in text.split():
        if 'https' in word:
            link_counter += 1
        elif '#' in word:
            tags_counter += 1
        elif '@' in word:
            annot_counter += 1
        else:
            lst.append(word)
        if any(p in word for p in punctuation):
            punctuation_counter += 1
        if any(s.lower() == word.lower() for s in slang_words):
            slang_counter += 1
    if link_counter > 0:
        link_counter -= 1
    word_counter = len(lst)
    out_text = ' '.join(lst)
    text_length = len(out_text)
    return emoji_counter, slang_counter, punctuation_counter, tags_counter, annot_counter, link_counter, word_counter, text_length, out_text


def query_extract(query):
    joined_query = ' '.join(*query)
    target_lst = list[str]()
    semantic_lst = list[str]()
    for count, word in enumerate(joined_query.strip("[['()']]").split()):
        if count == 0 or '#' in word:
            target_lst.append(word)
        else:
            if word != 'OR':
                semantic_lst.append(word)
    target = ' '.join(target_lst)
    semantic = ' '.join(semantic_lst)
    return target, semantic


def make_score(likes, retweets, quotes, replies, followers):
    score = 0.25*likes + 0.45*retweets + 0.15*(quotes + replies)
    try:
        return score/followers
    except ZeroDivisionError:
        return score


